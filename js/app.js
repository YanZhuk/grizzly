import '../css/app.scss';
import $ from "jquery";
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';


/**
 * EXAMPLE JS STARTS
 */
$(function() {
    $("#showmap").on("click", function(ev) {
        $( ".map" ).toggle( "slow", function() {
            // Animation complete.
        });
    })
    $("button[type='modal']").on("click", function(ev) {
        $('#myModal').modal({})
    });
});
/**
 * EXAMPLE JS ENDS
 */
